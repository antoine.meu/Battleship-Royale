﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        [Header("Game Object References")]
        public CameraMapController CameraController;
        public GravityController GravityController;
        public List<TrajectoryController> Trajectories;
    }
}